<?php

class Router {

    private $applicationElements = array(); // holds information about the controller, action, and parameters
    private $model = array();   // helps loading the models

    /** \Request $request */
    private $request;

    public function __construct(\Request $request) {
        $this->request = $request;
        $this->applicationElements['controller'] = $request->path(0);
        $this->applicationElements['action'] = $request->path(1);
        $this->applicationElements['var'] = $request->path(2);
        $this->init();
    }

    protected function init() {
        if (!empty($this->applicationElements['controller'])) {
            $controller = $this->applicationElements['controller'];
        } else {
            $controller = 'game';
        }
        $this->loadController($controller);
    }
    
    protected function loadController($controllerName) {

        $controllerName = ucfirst($controllerName);
        // get the name of the file that holds the controller
        $controllerFile = "application/controllers/{$controllerName}.php";
        // if that file doesn't exist die
        if (!file_exists($controllerFile)) {
            die('ERROR: Controller file does not exist! Create <b>' . $this->applicationElements['controller'] . '.php</b> file in <b>application/controller/</b> folder.');
            header("Location: " . BASE_URL);
        }
        // load the controller file
        require_once($controllerFile);

        $controllerClass = $controllerName . "Controller";
        $controller = new $controllerClass($this->request);

        // if the requested method does not exist, load the index method
        if (method_exists($controller, $this->applicationElements['action'])) {
            $controller->{$this->applicationElements['action']}($this->applicationElements['var']);
        } else {
            $controller->index();
        }
    }

    
    // a simple redirector
    public function redirect($location) {
        header("Location: " . BASE_URL . '/' . $location);
    }

}
