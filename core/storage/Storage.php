<?php

/**
 * Description of Storage
 *
 * @author George Jinga
 */
abstract class Storage {

    protected $data = array();

    /**
     * @param mixed $data
     */
    protected function setData($data) {
        $this->data = $data;
    }

    /**
     * @return array
     */
    protected function getData() {
        return $this->data;
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return \Storage
     */
    public function set($key, $value) {
        $this->data[$key] = $value;
        $this->save();
        return $this;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function get($key) {
        $return = null;
        $this->load();
        if (array_key_exists($key, $this->data)) {
            $return = $this->data[$key];
        }

        return $return;
    }

    abstract public function save();
    
    abstract public function load();
}
