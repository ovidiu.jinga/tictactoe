<?php

require_once('../../application/configs/config.php');
require_once('../../core/FrontController.php');
require_once('../../core/Router.php');
require_once('../../core/Request.php');
require_once('../../core/storage/Storage.php');
require_once('../../core/storage/SessionStorage.php');
require_once('../../application/models/Board.php');
require_once('../../application/models/Game.php');
require_once('../../application/controllers/Game.php');

