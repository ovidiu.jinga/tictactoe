<?php

class BoardTest extends PHPUnit_Framework_TestCase {
    
    public $storage;
    
    public function setUp() {
        $storage = $this->getMock('SessionStorage');
        $storage->expects($this->any())
            ->method('save')
            ->will($this->returnValue(true));
        $this->storage = $storage;
    }
    
    public function testGetCellValue() {
        $board = new Board();
        $board->setStorage($this->storage);
        $board->setBoard(array(
            array(1, 2, 3),
            array(4, 5, 6),
            array(7, 8, 9),
        ));
        
        $this->assertEquals(1, $board->getCellValue(0, 0));
        $this->assertEquals(2, $board->getCellValue(0, 1));
        $this->assertEquals(3, $board->getCellValue(0, 2));
    }
}