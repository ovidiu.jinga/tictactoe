<?php
/*
 * Configuration file
 */

	// Base path, used mainly for creating urls
//	define("BASE_URL", "http://tictactoe.dev");
	define("BASE_URL", "");
	
	// Credentials for connecting to the database 
	$db['host'] 	= '';  // the host for the mysql database
	$db['dbname'] 	= ''; // the database name
	$db['user'] 	= '';		// the username
	$db['password'] = '';	// the password
	
	if(!empty($db['host'])){
        // create link to mysql server
        $link = mysql_connect($db['host'],$db['user'],$db['password']);
        if (!$link) {
            die('Could not connect to database: ' . mysql_error());
        }

        // select the database
        mysql_select_db($db['dbname']);
    }
