<?php

/**
 * Description of Board
 *
 * @author George Jinga
 */
class Board {

    const BOARD_DIMENSION = 3;

    /** array $board */
    private $board;

    /** \Storage $storage */
    private $storage;

    public function getStorage() {
        return $this->storage;
    }

    /**
     * @param Storage $storage
     */
    public function setStorage($storage) {
        $this->storage = $storage;
        return $this;
    }

    /**
     * @param array $board
     */
    public function setBoard($board) {
        $this->board = $board;
    }

    public function initBoard() {
        if (!isset($this->storage)) {
            throw new Exception("No storage system set for board.");
        }
        $board = $this->storage->get('board');
        if (!is_null($board)) {
            $this->board = $board;
        } else {
            $this->resetBoard();
        }
    }

    /**
     * Reset all the values on the board
     */
    public function resetBoard() {
        for ($i = 0; $i < self::BOARD_DIMENSION; $i++) {
            for ($j = 0; $j < self::BOARD_DIMENSION; $j++) {
                $this->board[$i][$j] = 0;
            }
        }
        $this->saveBoard();
    }

    /**
     * Save board values to storage
     */
    private function saveBoard() {
        $this->storage->set('board', $this->board);
    }

    /**
     * @param integer $line
     * @param integer $column
     * @param integer $value
     */
    public function setMove($line, $column, $value) {
        $this->board[$line][$column] = $value;
        $this->saveBoard();
    }

    /**
     * @param integer $line
     * @param integer $column
     * @return integer
     */
    public function getCellValue($line, $column) {
        return $this->board[$line][$column];
    }

    /**
     * Calculates the sums for all lines on the board
     * @return array Contains array with sums for all the lines
     */
    public function sumAllLines() {
        $sum = array(
            'diag1' => 0,
            'diag2' => 0,
            'lin0' => 0,
            'lin1' => 0,
            'lin2' => 0,
            'col0' => 0,
            'col1' => 0,
            'col2' => 0,
        );

        for ($i = 0; $i <= 2; $i++) {
            for ($j = 0; $j <= 2; $j++) {
                $sum['lin' . $i] += $this->board[$i][$j];
                if ($i == $j) {
                    $sum['diag1'] += $this->board[$i][$j];
                }

                if (($i + $j) == 2) {
                    $sum['diag2'] += $this->board[$i][$j];
                }
            }
        }

        for ($j = 0; $j <= 2; $j++) {
            for ($i = 0; $i <= 2; $i++) {
                $sum['col' . $j] += $this->board[$i][$j];
            }
        }

        return $sum;
    }

    /**
     * returns an array with the coordinates of all the empty cells
     * @return array
     */
    public function getEmptyCells() {
        $emptyCells = array();
        for ($i = 0; $i <= 2; $i++) {
            for ($j = 0; $j <= 2; $j++) {
                if ($this->getCellValue($i, $j) == 0) {
                    $emptyCells[] = array(
                        'line' => $i,
                        'column' => $j
                    );
                }
            }
        }

        return $emptyCells;
    }

}
