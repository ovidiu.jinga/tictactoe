<?php

/**
 * Description of Game
 *
 * @author George Jinga
 */
class Game {

    const SCORE_PLAYER_WIN = 6;
    const SCORE_COMPUTER_WIN = 15;
    const POINTS_PLAYER = 2;
    const POINTS_COMPUTER = 5;

    /** @var Board */
    private $Board;

    /** @var integer $moves */
    private $moves;

    /** @var Storage $storage */
    private $storage;

    public function getStorage() {
        return $this->storage;
    }

    public function setStorage($storage) {
        $this->storage = $storage;
    }

    public function getNumberOfMoves() {
        return $this->moves;
    }

    public function setMoves($moves) {
        $this->moves = $moves;
        $this->saveGame();
    }

    public function getBoard() {
        return $this->Board;
    }

    public function setBoard(Board $Board) {
        $this->Board = $Board;
    }

    public function saveGame() {
        $this->storage->set('moves', $this->moves);
    }

    public function initGame() {
        if (!isset($this->storage)) {
            throw new Exception("No storage system set for game.");
        }
        $moves = $this->storage->get('moves');
        if (!is_null($moves)) {
            $this->setMoves($moves);
        } else {
            $this->resetGame();
        }
    }

    public function resetGame() {
        $this->setMoves(0);
        $this->getBoard()
            ->resetBoard();
    }

    public function incrementMoves() {
        $this->moves++;
        return $this;
    }

    /**
     * Check the board to see if current user won
     * @return bool
     */
    public function checkPlayerWin() {
        $linesSum = $this->getBoard()
            ->sumAllLines();
        foreach ($linesSum as $sum) {
            if ($sum == self::SCORE_PLAYER_WIN) {
                return true;
            }
        }

        return false;
    }

    public function checkDraw() {
        return $this->getNumberOfMoves() >= 9;
    }

    /**
     * Checks if computer is in the situation to win
     * @return bool
     */
    public function getWinningMove($points) {
        $return = null;
        $board = $this->getBoard();
        // calculate the sums for all importatnt rows
        $linesSum = $board->sumAllLines();

        // check each row
        foreach ($linesSum as $line => $sum) {
            if ($sum == $points * 2) {
                // see which line
                switch ($line) {
                    case 'lin0' : {
                            for ($j = 0; $j <= 2; $j++) {
                                if ($board->getCellValue(0, $j) == 0) {
                                    $return = array(
                                        'line' => 0,
                                        'column' => $j
                                    );
                                    break;
                                }
                            }
                            break;
                        }
                    case 'lin1' : {
                            for ($j = 0; $j <= 2; $j++) {
                                if ($board->getCellValue(1, $j) == 0) {
                                    $return = array(
                                        'line' => 1,
                                        'column' => $j
                                    );
                                    break;
                                }
                            }
                            break;
                        }
                    case 'lin2' : {
                            for ($j = 0; $j <= 2; $j++) {
                                if ($board->getCellValue(2, $j) == 0) {
                                    $return = array(
                                        'line' => 2,
                                        'column' => $j
                                    );
                                    break;
                                }
                            }
                            break;
                        }
                    case 'col0' : {
                            for ($i = 0; $i <= 2; $i++) {
                                if ($board->getCellValue($i, 0) == 0) {
                                    $return = array(
                                        'line' => $i,
                                        'column' => 0
                                    );
                                    break;
                                }
                            }
                            break;
                        }
                    case 'col1' : {
                            for ($i = 0; $i <= 2; $i++) {
                                if ($board->getCellValue($i, 1) == 0) {
                                    $return = array(
                                        'line' => $i,
                                        'column' => 1
                                    );
                                    break;
                                }
                            }
                            break;
                        }
                    case 'col2' : {
                            for ($i = 0; $i <= 2; $i++) {
                                if ($board->getCellValue($i, 2) == 0) {
                                    $return = array(
                                        'line' => $i,
                                        'column' => 2
                                    );
                                    break;
                                }
                            }
                            break;
                        }
                    case 'diag1' : {
                            for ($i = 0; $i <= 2; $i++) {
                                if ($board->getCellValue($i, $i) == 0) {
                                    $return = array(
                                        'line' => $i,
                                        'column' => $i
                                    );
                                    break;
                                }
                            }
                            break;
                        }
                    case 'diag2' : {
                            for ($i = 0; $i <= 2; $i++) {
                                if ($board->getCellValue($i, 2 - $i) == 0) {
                                    $return = array(
                                        'line' => $i,
                                        'column' => 2 - $i
                                    );
                                    break;
                                }
                            }
                            break;
                        }
                    default: break;
                }
            }
        }

        return $return;
    }

    public function getComputerMove() {
        $return = array();
        // check if computer can win
        if (!is_null($computerMove = $this->getWinningMove(self::POINTS_COMPUTER))) {
            $return['cell'] = $computerMove;
            $return['message'] = 'Computer wins!';
        // if it cannot win check if it can block the player from winning
        } else if (!is_null($computerBlock = $this->getWinningMove(self::POINTS_PLAYER))) {
            $return['cell'] = $computerBlock;
            $return['message'] = 'Computer blocks!';
        // pick a random cell
        } else {
            $emptyCells = $this->getBoard()->getEmptyCells();
            $cell = array_rand($emptyCells, 1);
            $return['cell'] = $emptyCells[$cell];
            $return['message'] = 'Computer doesn\'t have a winning move yet.';
        }

        return $return;
    }

    /**
     * @param integer $line
     * @param integer $column
     * @param integer $value
     * @return \Game
     */
    public function registerMove($line, $column, $value) {
        $board = $this->getBoard();
        $board->setMove($line, $column, $value);
        $this->incrementMoves();
        $this->saveGame();
        return $this;
    }
}
