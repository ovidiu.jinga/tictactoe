// TODO: move this to CSS
$('div.cell').hover(
    function(){
        if($(this).html()==''){
            $(this).css('background-color','#EEEEEE');
        }
    },
    function(){
        $(this).css('background-color','#FFFFFF');
    }
);

$('div.cell').click(function(){
    // if the cell is empty send ajax
    if($(this).html() == ''){
        $(this).html('X');
        $.ajax({
            url: 'http://tictactoe.dev/game/playerMove/',
            type: 'POST',
            dataType: 'JSON',
            data: {'id': $(this).attr('id')},
            success: function(data){
                $('div#'+data.cell).html('O');
                $('div#messages').html(data.message);
                // TODO: implement disabling cells on win - add class disabled to all cells
            }
        });
    }
});

$('a#play_again').click(function(element){
    element.preventDefault();
    $.ajax({
        url: 'http://tictactoe.dev/game/playAgain/',
        type: 'POST',
        dataType: 'JSON',
        success: function(data) {
            Board.resetBoard();
        }
    })
});

Board = {
    resetBoard: function(){
        $('div.cell').html('');
    }
}