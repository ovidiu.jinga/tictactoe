<?php

// TODO: add bootstrapping / autoloading
// TODO: add unit tests
// load the configuration file - contains database connection credentials
require_once('application/configs/config.php');
require_once('core/FrontController.php');
require_once('core/Router.php');
require_once('core/Request.php');
require_once('core/storage/Storage.php');
require_once('core/storage/SessionStorage.php');

$request = new Request();
$router = new Router($request);
